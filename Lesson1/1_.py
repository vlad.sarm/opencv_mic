import cv2 as cv
import matplotlib.pyplot as plt
import numpy as np

'''
pip install numpy
pip install matplotlib
pip install opencv-python

Чтобы запустить pip с нужным интерпретатором:
C:/Users/Vlad.Sarm/AppData/Local/Programs/Python/Python36-32/python.exe \
    -m pip install {smth}
'''

# Способы создания массивов с помощью NumPy

# Первый параметр - обычный список Python или другой массив
a = np.array([
    [1, 2, 3],
    [4, 5, 7],
    [23,45,87]
    ])

# [от, до), шаг
b = np.arange(5, 25, 2)

# [от, до], кол-во элементов
l = np.linspace(0, np.pi, 10)

# Массив из единичек заданного размера
ones = np.ones((4,4))

# Массив из нулей заданного размера
zeros = np.zeros((4,4))

# Массив из нулей такой же размерности как и параметр
zeros2 = np.zeros_like(ones)

# Массив из единиц такой же размерности как и параметр
ones2 = np.ones_like(ones)

# Массив необходимой размерности заполненный нужным элементом
f = np.full((2,2,2), 2)

# Пустой массив, эл-ты не определены
em = np.empty((3,5))

# Поменять размерность массива
b = np.arange(5, 25, 2).reshape(5, 2)

# Основные св-ва: размер, кол-во эл-ов, тип эл-ов, размерность, размерность 
print(b.shape, b.size, b.dtype, b.ndim, len(b.shape))

#4 способа задания типа данных
b2 = np.array(a).astype('uint8')
b3 = np.array(a, dtype='uint8')
b4 = np.array(a, dtype=np.uint8)

b5 = np.array(a, dtype=int) # другой тип


#Сохранить в бинарном виде
np.save('res/test', b)

#Сохранение нескольких массивов в 1 файл
np.savez('res/test_a_b', a, b)

#Сохранение в текстовом виде
np.savetxt('res/test_txt', b, delimiter=" ")

#Загрузка (аналогично для других сохранений)
t = np.loadtxt('res/test_txt')

# Копия массива
t2 = t.copy()
