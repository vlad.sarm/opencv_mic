import numpy as np
import matplotlib.pyplot as plt


# Floyd–Steinberg dithering 
# https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwjw2_mr_-b6AhUmlYsKHf1wBEQQFnoECA0QAQ&url=https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FFloyd%25E2%2580%2593Steinberg_dithering&usg=AOvVaw2MwnWSDs9at5eTgkXCfqnM
def dith(m):
    m2 = m.copy()

    for i in range(1, m.shape[0] - 1):
        for u in range(1, m.shape[1] - 1):
            old_pix = m2[i, u]
            tresh = 255 // 2
            new_pix = (0 if old_pix < tresh else 255)
            error = old_pix - new_pix

            m2[i , u + 1] += int(error*7/16)
            m2[i + 1 , u - 1] += int(error*3/16)
            m2[i + 1 , u] += int(error*5/16)
            m2[i + 1 , u + 1] += int(error*1/16)

            m2[i, u] = new_pix

    return m2


img = plt.imread('media/guido.jpg')

# Одна строка, 4 столбца
fig, ax = plt.subplots(1, 4)

img[10:40, 10:40, :] = 255

b = img[:,:,2]
g = img[:,:,1]
r = img[:,:,0]

b2 = dith(b)
g2 = dith(g)
r2 = dith(r)

# Создадим пустую копию картинки и запишим обработанные каналы, нормализуя их
img2 = np.empty(img.shape)
img2[:,:,0] = r2 // 255
img2[:,:,1] = g2 // 255
img2[:,:,2] = b2 // 255

plt.imsave('res/out2.png', img2)

ax[0].imshow(b2, cmap='gray')
ax[1].imshow(g2, cmap='gray')
ax[2].imshow(r2, cmap='gray')
ax[3].imshow(img2)

plt.show()
