import cv2 as cv
import numpy as np

def onMouseMove(e, x, y, flags, params):
    if e == cv.EVENT_MOUSEMOVE:
        str = f"{x} {y} {params[y,x]}"

        font = cv.FONT_HERSHEY_SIMPLEX

        disp = blank.copy()
        cv.putText(disp, str, (5, 15), font, 0.5, (255, 255, 255))
        cv.imshow("Img", disp)

blank = np.zeros((500, 500, 3), dtype='uint8')

blank = cv.rectangle(blank, (100,100), (400,400), (0, 220, 0), -1)
blank[100:200, 150:200, 2] = 200 

cv.imshow("Img", blank)

cv.setMouseCallback("Img", onMouseMove, param=blank)

cv.waitKey(0)
