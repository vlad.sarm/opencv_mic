import cv2 as cv

img  = cv.imread("media/guido.jpg")
cv.imshow("Orig", img)

gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
cv.imshow("gray", gray)

thresh, thresholded = cv.threshold(gray, 20, 255, cv.THRESH_BINARY)
cv.imshow("thresholded", thresholded) 

thresh_inv, thresholded_inv = cv.threshold(gray, 20, 255, cv.THRESH_BINARY_INV)
cv.imshow("thresholded_inv", thresholded_inv) 

adapt_gauss_thresh = cv.adaptiveThreshold(gray, 255, cv.ADAPTIVE_THRESH_GAUSSIAN_C, cv.THRESH_BINARY, 11, 5)
cv.imshow("adapt_gauss_thresh", adapt_gauss_thresh) 


adapt_mean_thresh = cv.adaptiveThreshold(gray, 255, cv.ADAPTIVE_THRESH_MEAN_C, cv.THRESH_BINARY, 11, 5)
cv.imshow("adapt_mean_thresh", adapt_mean_thresh) 

thresh, otsu = cv.threshold(gray, 0, 255, cv.THRESH_OTSU)
print(thresh)
cv.imshow("otsu", otsu) 

cv.waitKey(0)
