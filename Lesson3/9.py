import cv2 as cv
import numpy as np
import pickle

width, height = 107, 48

try:
    with open("carPark", "rb") as f:
        parkList = pickle.load(f)
except:
    parkList = []


capture = cv.VideoCapture("media\carPark.mp4")

cv.namedWindow("Trackbars")
cv.resizeWindow("Trackbars", 640, 240)

def empty(e):
    pass

cv.createTrackbar("Diam", "Trackbars", 15, 30, empty)
cv.createTrackbar("sCol", "Trackbars", 12, 30, empty)
cv.createTrackbar("sSpace", "Trackbars", 8, 30, empty)
cv.createTrackbar("bSize", "Trackbars", 8, 30, empty)
cv.createTrackbar("sThresh", "Trackbars", 5, 30, empty)
cv.createTrackbar("WhiteNum", "Trackbars", 1600, 2500, empty)

while True:

    if capture.get(cv.CAP_PROP_POS_FRAMES) == capture.get(cv.CAP_PROP_FRAME_COUNT):
        capture.set(cv.CAP_PROP_POS_FRAMES, 0)

    isTrue, frame = capture.read()

    assert isTrue

    gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
    
    d = cv.getTrackbarPos("Diam", "Trackbars")
    sCol = cv.getTrackbarPos("sCol", "Trackbars")
    sSpace = cv.getTrackbarPos("sSpace", "Trackbars")
    bSize = cv.getTrackbarPos("bSize", "Trackbars")
    if bSize % 2 == 0: bSize+=1
    sThresh = cv.getTrackbarPos("sThresh", "Trackbars")
    
    blured = cv.bilateralFilter(gray, d, sCol, sSpace)
    blured = cv.GaussianBlur(blured, (3,3), 3)
    thresholded = cv.adaptiveThreshold(blured, 255, cv.ADAPTIVE_THRESH_GAUSSIAN_C, \
        cv.THRESH_BINARY_INV, bSize, sThresh)

    ones = np.ones((2,2))
    eroded = cv.erode(thresholded, ones)
    
    dilated = cv.dilate(eroded, ones, iterations=3)

    spaces = 0
    whiteNum = cv.getTrackbarPos("WhiteNum", "Trackbars")
    
    for xy in parkList:
        croped = dilated[xy[1]:xy[1]+height, xy[0]:xy[0]+width]
        color = (0,0,220)
        whites = cv.countNonZero(croped)
        if whites <= whiteNum:
            spaces+=1
            color = (0,220,0)
        cv.rectangle(frame, (xy[0],xy[1]), (xy[0]+width,xy[1]+height), color, 2)

    cv.putText(frame, f"FREE {spaces}", (40,60), cv.FONT_HERSHEY_SIMPLEX, 2, (0,0,255), 2)
    cv.imshow("Parking", frame)


    if cv.waitKey(20) & 0xFF == ord("q"):
        break

capture.release()
