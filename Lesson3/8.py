import cv2 as cv
import numpy as np
import pickle

width, height = 107, 48

try:
    with open("carPark", "rb") as f:
        parkList = pickle.load(f)
except:
    parkList = []

img = cv.imread("media\carParkImg.png")
cv.imshow("Picker", img)

def onClick(event, x, y, flags, params):
    if event == cv.EVENT_LBUTTONDOWN:
        parkList.append((x, y))
    elif event == cv.EVENT_RBUTTONDOWN:
        for xy in parkList:
            if x >= xy[0] and x <= xy[0] + width and \
                y >= xy[1] and y <= xy[1] + height:
                parkList.remove(xy)

    with open("carPark", "wb+") as f:
        pickle.dump(parkList, f)

cv.setMouseCallback("Picker", onClick)

while True:
    img_copy = img.copy()
    for xy in parkList:
        cv.rectangle(img_copy, (xy[0], xy[1]), (xy[0] + width, xy[1] + height),\
            (0,0,255), 2)

    cv.imshow("Picker", img_copy)
    
    if cv.waitKey(20) & 0xFF == ord("q"):
        break
    
