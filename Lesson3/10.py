import cv2 as cv
import numpy as np

capture = cv.VideoCapture("media\carPark.mp4")
_, frame_old = capture.read()
gray_old = cv.cvtColor(frame_old, cv.COLOR_BGR2GRAY)
p0 = cv.goodFeaturesToTrack(gray_old, 100, 0.1, 10, blockSize=4)

color = np.random.randint(0, 255, (100, 3), dtype="uint8")

while True:
    if capture.get(cv.CAP_PROP_POS_FRAMES) == capture.get(cv.CAP_PROP_FRAME_COUNT):
        capture.set(cv.CAP_PROP_POS_FRAMES, 0)

    isTrue, frame = capture.read()
    gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
    #[[[x1, y1]], [[x2,y2]]]

    p1, state, err = cv.calcOpticalFlowPyrLK(gray_old, gray, p0, None)

    good_new = p1[state==1]
    good_old = p0[state==1]

    mask = np.zeros_like(frame)
    for i, (p1, p2) in enumerate(zip(good_new, good_old)):
        x1,y1 = p1.ravel().astype("int")
        x2,y2 = p2.ravel().astype("int")
        mask = cv.line(mask, (x1,y1), (x2,y2), color[i].tolist(), 2)
        frame = cv.circle(frame, (x1,y1), 7, color[i].tolist(), thickness=-1)

    img = cv.add(frame, mask)
    cv.imshow("FRAME", img)

    if cv.waitKey(20) & 0xFF == ord("q"):
        break

capture.release()
