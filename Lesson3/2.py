import cv2 as cv

img = cv.imread("media/guido.jpg")
cv.imshow("Orig", img)

blur = cv.blur(img, (3, 3))
cv.imshow("blur",blur)

gaussBlur = cv.GaussianBlur(img, (3, 3), 3)
cv.imshow("gaussBlur",gaussBlur)

median = cv.medianBlur(img, 7)
cv.imshow("median",median)

bilateral = cv.bilateralFilter(img, 10, 32, 53)
cv.imshow("bilateral",bilateral)

cv.waitKey(0)
