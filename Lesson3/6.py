import cv2 as cv
import numpy as np

img  = cv.imread("media/guido.jpg")
cv.imshow("Orig", img)

blank = np.zeros(img.shape[:2], dtype="uint8")
mask = cv.circle(blank, (img.shape[1]//2 - 20, img.shape[0]//2-150), 100, 255, -1)
masked = cv.bitwise_and(img, img, mask=mask)
cv.imshow("Res", masked)

cv.waitKey(0)
