import cv2 as cv

sigma = 1

def empty(e):
    pass

def onSigmaChanged(val):
    global sigma
    sigma = val

img = cv.imread("media/guido.jpg")
cv.imshow("Orig", img)

cv.namedWindow("Trackbars")
cv.resizeWindow("Trackbars", 640, 80)

cv.createTrackbar("Kernel", "Trackbars", 1, 20, empty)
cv.createTrackbar("Sigma", "Trackbars", 1, 20, onSigmaChanged)

while True:
    ker_val = cv.getTrackbarPos("Kernel", "Trackbars")
    if ker_val % 2 == 0 : ker_val+=1

    gaussBlur = cv.GaussianBlur(img, (ker_val, ker_val), sigma)
    cv.imshow("gaussBlur",gaussBlur)

    if cv.waitKey(20) & 0xFF == ord("q"):
        break

cv.destroyAllWindows()
