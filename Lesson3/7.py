import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt

img = (np.random.rand(20,20,3) * 255).astype('uint8')

def bin1(m):
    return (0 if m < 200 else 1)

toBin = np.vectorize(bin1)
img2 = toBin(img).astype("uint8")
bin = cv.cvtColor(img2, cv.COLOR_BGR2GRAY)

plt.figure(0)
plt.imshow(bin, cmap="gray")

plt.figure(1)
ker = np.ones((2,2))
dilated = cv.dilate(bin, ker, iterations=2)
plt.imshow(dilated, cmap="gray")

plt.figure(2)
eroded = cv.erode(bin, ker)
plt.imshow(eroded, cmap="gray")

plt.show()
