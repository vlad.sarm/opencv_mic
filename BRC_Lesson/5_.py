import numpy as np
import matplotlib.pyplot as plt
import cv2 as cv

def translate(img, x, y):
    tranMat = np.float32([
        [1, 0, x],
        [0, 1, y]
    ])
    w = int(img.shape[1])
    h = int(img.shape[0])
    dim = (w, h)
    return cv.warpAffine(img, tranMat, dim)



def rotate(img, angle, rotPoint=None):
    w = int(img.shape[1])
    h = int(img.shape[0])
    dim = (w, h)
    if not rotPoint:
        rotPoint = (w//2, h//2)

    
    rotMat = cv.getRotationMatrix2D(rotPoint, angle, 1.0)
    return cv.warpAffine(img, rotMat, dim)

src = np.array([[0, 0], [0, 100], [100, 0]]).astype(np.float32)
dest = np.array([[0, 0], [50, 100], [100, 50]]).astype(np.float32)

mat = cv.getAffineTransform(src, dest)

img = cv.imread("opencv_mic/media/guido.jpg")
cv.imshow("Orig", img)

translated = translate(img, 100, 100)
cv.imshow("translated", translated)


rotated = rotate(img, 30)
cv.imshow("rotated", rotated)


transformed = cv.warpAffine(img, mat, (img.shape[1], img.shape[0]))
cv.imshow("transformed", transformed)

cv.waitKey(0)