import numpy as np
import matplotlib.pyplot as plt
import cv2 as cv

def rescaleFrame(frame, scale=0.75):
    w = int(frame.shape[1]*scale)
    h = int(frame.shape[0]*scale)
    dim = (w, h)
    return cv.resize(frame, dim, interpolation=cv.INTER_AREA)

capture = cv.VideoCapture("opencv_mic\media\carPark.mp4")

while True:

    if capture.get(cv.CAP_PROP_POS_FRAMES) == capture.get(cv.CAP_PROP_FRAME_COUNT):
        capture.set(cv.CAP_PROP_POS_FRAMES, 0)

    isTrue, frame = capture.read()
    frame = rescaleFrame(frame, 0.3)
    cv.imshow("WEBCAM", frame)

    key = cv.waitKey(20)
    if key & 0xFF == ord("q"):
        break
    elif key == ord("w"):
        cv.destroyAllWindows() 

capture.release()
cv.destroyAllWindows() 
