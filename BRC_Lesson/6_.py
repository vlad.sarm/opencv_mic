import cv2 as cv
import numpy as np

blank = np.zeros((500,500, 3), dtype="uint8")

blank = cv.rectangle(blank, (50, 50), (200, 200), (255, 0, 0), thickness=-1)
blank = cv.circle(blank, (50, 50), 56, (255, 255, 0), thickness=3)

lines = np.array([[[0,0],[100,100]],[[50,50],[450,50]]])
blank = cv.polylines(blank, lines, 1 ,(0, 0, 255), thickness=2)


cv.imshow("Img", blank)

cv.waitKey()
