import numpy as np
import matplotlib.pyplot as plt
import cv2 as cv

img = cv.imread("opencv_mic/media/guido.jpg")
cv.imshow("Orig", img)

gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
cv.imshow("Gray", gray)


b, g, r = cv.split(img)
cv.imshow("Blue", b)

r = (r*0.31).astype("uint8")

merged = cv.merge([b, g, r])
cv.imshow("merged", merged)

blank = np.zeros_like(g)

blue = cv.merge([b, blank, blank])
cv.imshow("blue", blue)

if cv.waitKey(0) & 0xFF == ord("s"):
    cv.imwrite("oout.png", img)


