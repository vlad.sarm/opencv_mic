import matplotlib.pyplot as plt
import numpy as np

img = plt.imread('opencv_mic/media/guido.jpg')

fig, ax = plt.subplots(1, 4)

ax[0].imshow(img)

r = img[:, :, 0]
g = img[:, :, 1]
b = img[:, :, 2]

ax[1].imshow(r, cmap="gray")
ax[2].imshow(g)
ax[3].imshow(b)

plt.figure(0)

red = img.copy()
red[:,:,1:3] = 0

plt.imshow(red)
plt.show()
