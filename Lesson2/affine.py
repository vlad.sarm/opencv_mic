import numpy as np
import cv2 as cv

img = cv.imread('media/guido.jpg')

def translate(img, x, y):
    transMat = np.array(
        [
            [1,0,x],
            [0,1,y]
            ]
        , dtype='float32')
    dim = (img.shape[1], img.shape[0])

    return cv.warpAffine(img, transMat, dim)

translated = translate(img, -200, -100)

def rotate(img, angle, rotPoint=None):
    dim = (img.shape[1], img.shape[0])
    if rotPoint is None:
        rotPoint = (dim[0] // 2, dim[1] // 2)
    rotMap = cv.getRotationMatrix2D(rotPoint, angle, 1)
    print(rotMap)
    return cv.warpAffine(img, rotMap, dim)

rotated = rotate(img, 30, (0,0))

cv.imshow('Orig', img)
cv.imshow('rotated', rotated)
cv.imshow('translated', translated)
w = img.shape[1]
h = img.shape[0]
orig = np.array([[0,0], [w//2, 0], [0, h //2]], dtype='float32')
res = np.array([[0,0], [w//2, 100], [100, h //2]], dtype='float32')

mat = cv.getAffineTransform(orig, res)
print(mat)
transformed = cv.warpAffine(img, mat, (w, h))
cv.imshow('transformed', transformed)

cv.waitKey()
