import cv2 as cv

capture = cv.VideoCapture(0)

def scale(frame, scale=0.5):
    w = int(frame.shape[1] * scale)
    h = int(frame.shape[0] * scale)
    dim = (w, h)

    return cv.resize(frame, dim, interpolation=cv.INTER_LINEAR)

def changeRes(w, h):
    capture.set(3, w)
    capture.set(4, h)

# changeRes(300,300)

while True:
    if capture.get(cv.CAP_PROP_POS_FRAMES) == capture.\
        get(cv.CAP_PROP_FRAME_COUNT):
        capture.set(cv.CAP_PROP_POS_FRAMES, 0)

    isTrue, frame = capture.read()
    frame = scale(frame, scale=3)
    cv.imshow('Video', frame)

    if cv.waitKey(10) & 0xFF == ord('q'):
        break

capture.release()
