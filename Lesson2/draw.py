import cv2 as cv
import numpy as np

black = np.zeros((400,400, 3), dtype='uint8')

cv.rectangle(black, (50,50), (150,150), (140,24,13), thickness=-1)
cv.circle(black, (200, 200), 70, (255, 255, 255), thickness=4)

pts = np.array([[25, 70], [25, 145], [75, 190]], dtype=np.int32)

cv.polylines(black, [pts, pts*2], 1, (0,0,255), thickness=1)

cv.imshow('Blank', black)
cv.waitKey()
