import cv2 as cv
import numpy as np

img = cv.imread('media/guido.jpg')

# print([i for i in dir(cv) if i.startswith('COLOR_')])

gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

b = img[:,:,0]
b,g,r = cv.split(img)
cv.imshow('Blue', b)

zeros = np.zeros(img.shape[:2], dtype='uint8')
blue = cv.merge([b, zeros, zeros])
cv.imshow('Blue2', blue)


cv.imshow('Gray', gray)
cv.imshow('Orig', img)
cv.waitKey()

cv.imwrite('res/guido_gray.png', img)
